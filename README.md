# Coding Challenge 
_ Organisé par Le Repaire du Web_

🏆  __**Concours #8 : Développement créatif **__  🏆 

Début du concours     :   **Mardi 20 Juin,   11h00.**
Fin des participations :  **Mardi 4 Juillet,  23h59.**

**Récompenses**
Les participants qui atteignent le podium reçoivent via Paypal :

**1ère place **   : 100€
**2ème place** : 50€
**3ème place** : 25€


_** ⚠️  __ATTENTION__ : ⚠️ **_

Votre travail doit être **ORIGINAL** et **UNIQUE**, autant au niveau du **DESIGN** que du **CODE**.
**Aucune ressemblance avec quoique ce soit sur le web n'est tolérée, sous peine d'être exclu du concours.**
Partez d'une feuille blanche et tentez de tout créer de A à Z, on doit pouvoir sentir instantanément que tout vient de vous.

Votre création doit également être faite spécialement pour le concours, dans le **temps imparti.**
**Il est interdit d'utiliser une création qu'on aurait déjà faite, et de participer à un concours avec.**
Ce serait injuste envers ceux qui participent dans le temps imparti.


__**Description :**__

Le développement créatif est un domaine dans lequel les limites de l'UI/UX et des animations et interactions web sont repoussées.

Le but n'est **pas que** d'en mettre plein la vue, il faut surtout respecter un thème, une idée, une image qu'on va mettre en valeur avec ses talents web / graphiques.
Vous devez représenter une marque, une cause, un client, une entreprise de la meilleure façon possible.
Une identité visuelle doit être mise en place, une charte graphique doit être respectée, c'est un problème à résoudre comme on résout un problème logique.
Enfin, ne faîtes pas l'impasse sur l'aspect fonctionnel de votre création.
On croit souvent, à tort, que c'est impossible de créer quelque chose de beau, stylé et innovant sans détruire l'expérience utilisateur.
Prouvez le contraire en prenant soin de l'accessibilité de vos créations, dans la limite du possible.

**1.** Première étape, trouvez un thème :

Cherchez une cause à défendre, un domaine d'entreprise à représenter ou simplement une idée originale qui a beaucoup de potentiel créatif.
N'hésitez pas à inventer une entreprise ou une organisation fictive.

Scrollez des sites comme Awwwards si vous n'avez pas d'idée et inspirez-vous des thèmes que vous voyez passer. (attention, le plagiat est interdit et entraîne une exclusion).

**2.** Seconde étape, créez un site en fonction de votre idée.

Je vous conseille fortement de créer une identité graphique sur Figma ou autre en premier.
Faîtes des tests, essayez quelques palettes graphiques, créez quelques composants, etc ...
Néanmoins, les logiciels de Design sont parfois incapables de représenter certaines créations web, comme des environnements 3D ou certaines animations au scroll, vous devrez les créer en dehors de tout ça, sur un IDE. 
 

__**Technologies :**__

Vous êtes libres d'utiliser toutes les technologies web que vous souhaitez.

N'oubliez pas que, parfois, une petite interaction CSS peut avoir un très bon rendu et être très adaptée à un problème donné.

Ne vous sentez pas obligés d'utiliser des technos que vous ne maîtrisez pas sous prétexte que ça vous rapporterait plus de points.

Idées de technos/techniques à utiliser :
- Animations au scroll JS (observer, scroll event, ...)
- Animations au scroll CSS (bg-fixed, scroll-snap, etc...)
- WebGL (ThreeJS, Spline, etc...)
- Utiliser le Canvas nativement
- Animation / interaction CSS
- Validation de données animée côté front
- SVGs
- RequestAnimationFrame
- Les évènements du DOM
- Etc ...

**Créer/Personnaliser ses propres médias améliorera énormément votre classement.**

Ce n'est pas la photo de fond de XXX trouvée sur Unsplash qui est notée, mais **votre création** ou le rapport judicieux qu'elle a avec celle-ci. 👍


__**Règles :**__

Tout est expliqué en détail ici : https://discord.com/channels/655077317911117860/1041772919266750616

*Bon code, soyez créatif et amusez-vous ! <:icon_lrdw:951077761169113098>🚀*


# Solution Apportée 

Dans un monde fictif, la société @SunburnDynasty est en pleine croissance. 
Michel Dupont & Camille Leroi sont de très bons amis. Des amis d'enfances qui se connaissent par coeur. Il y a quelques mois, alors que comme chaque année, ils essayaient tant bien que mal de s'organiser pour partir ensemble avec leurs conjoints respectifs en vacances, ils eurent une idée de génie. Celle qui faciliterait l'organisation à plusieurs de voyage entre ami. Facilitant la réservation, la concertation, et la validation de chaque étape d'un voyage. 

Hier encore, ils travaillaient dans le garage de Camille, aujourd'hui la société compte 7 autres employés. 

Direction
* Michel Dupont Co-fondateur et responsable commercial
* Camille Leroi Co-fondatrice et responsable marketing

Employés
* Margaret Techer       Chargée de rectrutement
* Alfred Guilbert       Chargée d'affaires
* Helene Dumas          Assistante aux 
* Laurence Meyer        Développeuse Marketing
* Thomas Ferrand        Community Manager
* Kevin Couturier       Community Manager
* Jeanne Dumas          Graphiste

L'équipe étant constamement en déplacement ou en télétravail. Michel et Camille ont fait commande d'une application Web, permettant de partager des applications dédiées aux métiers de chacun des employés, ainsi que de centraliser les informations appartenant à @SunburnDynasty. 

C'est cet outil Deskito, qui sera réalisé ici.

# Technologie 

Dans un but pédagogique, l'application Frontend se limitera en terme de librairies JavaScript. 

La base du projet utilisera React JS [v.18.2.0]

Par confort, le backend sera développé en Java [v.17] avec un serveur embarqué Tomcat & configuré avec SpringBoot [v.3]

# Annexes - Ressources & Documentations
Les sites suivants ont été utilisé pour les illustrations / icones / themes. 

Undraw --> https://undraw.co/
